from attr import attrs, attrib, Factory
from slugify import slugify
from typing import Dict

@attrs
class C:
    title : str = attrib()
    description : str = attrib()
    slug : str = attrib()
    body : str = attrib(default='')
    custom : Dict = attrib(default=Factory(dict))
    taxonomies : Dict = attrib(default=Factory(dict))

    @slug.default
    def __something(self):
        return slugify(self.title)