from cli.Gen import generatorFactory
from pathlib import Path, PurePosixPath
from pprint import pprint
from attr import asdict
from shutil import copytree, rmtree
from functools import partial

pages, taxonomies, env = generatorFactory(Path('content'))

output_dir = Path('output')
assets_dir = Path('content/includes')


root = PurePosixPath('/')
index = PurePosixPath('.')

if(output_dir.exists()):
    rmtree(output_dir)
    
copytree(assets_dir, output_dir)

def page_taxonomy_filter(taxonomy_item, taxonomy_name, page):
    taxonomy = page.taxonomies.get(taxonomy_name, None)
    if isinstance(taxonomy, list):
        return taxonomy_item in taxonomy
    elif isinstance(taxonomy, str):
        return taxonomy_item == taxonomy
    return False

def get_children(taxonomy_item, taxonomy_name, pages):
    the_filter = partial(page_taxonomy_filter, taxonomy_item, taxonomy_name)
    return filter(the_filter, pages)

for page in filter(lambda p: not p.hidden, pages):
    if(page.typ == 'taxonomy-item'):
        children = partial(get_children, page.name, page.taxonomy, pages)
        print(list(children()))
    template = env.get_template(page.template)
    result = template.render(page=page, children=children)
    file_path = (root / page.base_url / page.slug).relative_to('/')
    file_path = 'index.html' if file_path == index else file_path.with_suffix('.html').as_posix()
    output_file = output_dir / file_path
    output_file.parent.mkdir(parents=True, exist_ok=True)
    output_file.write_text(result, encoding='utf-8')