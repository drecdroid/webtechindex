from jinja2 import Environment

env = Environment()

class A():
    a = 10

    def fn(self):
        return self.a

template = env.from_string('''{{fn()}}''')
result = template.render(fn=A().fn)
print(result)