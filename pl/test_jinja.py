from jinja2 import Environment, loaders, Template
from attr import attrs, attrib, Factory

@attrs
class C():
    a = attrib(default=10)
    
    def call(self, a):
        print("a")


env = Environment()

_globals = {
    'title' : 'global title'
}

env.globals.update(_globals)

template : Template = env.from_string('{{title}}')

rendered = template.render(title='overwritten title')


print(rendered)