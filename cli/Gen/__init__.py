from pathlib import Path, PurePosixPath
from typing import Callable, Dict, List, Union, AnyStr

from functools import partial
from attr import Factory, attrib, attrs, fields
from jinja2 import Environment
from jinja2.loaders import FileSystemLoader
from jinja2_markdown import MarkdownExtension

from ruamel.yaml import YAML

from slugify import slugify
from .pages import build_pages

from shutil import copytree, rmtree
from .utils import try_yaml


ROOT = PurePosixPath('/')
INDEX = PurePosixPath('.')

yaml = YAML(typ='safe')

def generatorFactory(content_dir: Path, output_dir : Path):
    in_content = partial(Path, content_dir)

    # dirs
    dirs = ['includes', 'templates', 'pages']
    
    includes_dir = in_content('includes')
    templates_dir = in_content('templates')
    pages_dir = in_content('pages')
    taxonomies_dir = in_content('taxonomies')


    # setup template environment
    loader = FileSystemLoader(templates_dir.as_posix())
    env = Environment(loader=loader)

    # TODO: load taxonomies settings
    settings_maybe_file = in_content('settings')
    settings = try_yaml(settings_maybe_file)
    
    # load global defined variables
    env.globals.update(settings.get('globals', {}))
    env.add_extension(MarkdownExtension)

    # load single pages
    pages_settings = settings.get('pages', {})
    pages = build_pages(pages_settings, pages_dir)

    if(output_dir.exists()):
        rmtree(output_dir)
        
    copytree(assets_dir, output_dir)

    for page in filter(lambda p: p.visible, pages):
        template = env.get_template(page['template'])
        result = template.render(page=page)
        file_path = (ROOT / page['base_url'] / page['slug']).relative_to('/')
        file_path = file_path.with_suffix('.html').as_posix()
        output_file = output_dir / file_path
        output_file.parent.mkdir(parents=True, exist_ok=True)
        output_file.write_text(result, encoding='utf-8')