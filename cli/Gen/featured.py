from pathlib import Path
from functools import partial
from toolz.functoolz import curry, flip
from operator import contains
from os.path import isfile
from sidekick import fn, _
from typing import Union

EXTS = ['.jpeg', '.jpg', '.png', '.mp4']


def and_fns(*fns):
    return lambda x: all(map(lambda y: y(x), fns))

def get_featured(p_dir : Union[Path, str]):
    p_dir = Path(p_dir)

    in_exts = lambda file: file.suffix.lower() in EXTS 
    is_featured = lambda file: file.stem == 'featured'

    condition = and_fns(isfile, in_exts, is_featured)

    featured = filter(condition, p_dir.iterdir())
    
    return next(featured, None)