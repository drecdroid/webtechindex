from pathlib import Path, PurePosixPath
from ruamel.yaml import YAML
from .featured import get_featured
from slugify import slugify
from functools import partial

ROOT = PurePosixPath('/')
yaml = YAML(typ='safe')

def get_path(page):
    base = PurePosixPath(page.get('base_url',''))
    name = page.get('slug', '')
    return (ROOT / base / name).as_posix()

def build_pages(rules : dict, pages_dir : Path):
    pages : dict = {}
    existent_urls : dict = {}

    for page_path in pages_dir.glob('**/page.y*ml'): # type: Path
        page_dir : Path = page_path.parent

        base = dict(
            visible = True,
            base_url  = Path( ROOT, page_dir.as_posix())
        )

        # Extending from rules
        page : dict = {}
        for folder in reversed(page_path.parents): # type: Path
            page : dict = {**page, **rules.get(folder.as_posix(), {})}

        map(lambda x: rules.get(folder.as_posix(), {}),reversed(page_path.parents))

        (rules[k] for k in reversed(page_path.parents) if k in rules)

        # Loading from disk
        page : dict = {**page, **yaml.load(page_path.read_text(encoding='utf-8'))}
        

        # Add title
        if page.get('title'):
            page.setdefault('slug', slugify(page['title']))
        else:
            print('Missing title')
        
        # Add id
        page_id = page_path.relative_to(pages_dir).parent.as_posix()
        page['id'] = page_id
        if not page_id in pages:
            pages[page_id] = page
        else:
            print(f'page with id={page_id} already exist')

        # Add body
        body_file : Path = page_dir / 'body.md'
        if body_file.exists():
            body_text = body_file.read_text(encoding='utf-8')
            page.setdefault('body', body_text)
        
        # Adding featured image
        featured_path = featured(page_dir)
        if featured_path:
            page['featured'] = featured_path
        
        page['path'] = partial(get_path, page)
        page.setdefault('groups', dict())
        group_pages(pages)

    return pages

def group_pages(pages : dict):
    for page_id, page in pages.items():
        groups = page['groups']
        for group_name, group in groups.items():
            grouped = []
            for item in group:
                item_page = pages.get(item)
                if item_page and not item == page_id:
                    grouped.append(item_page)
                elif item == page_id:
                    print(f'Page={page_id} can\'t be in its same groups')
                else:
                    print(f'Page with id={item}, in page={page_id}')
            groups[group_name] = grouped
                    
            