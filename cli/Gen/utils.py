'''
Some utility functions
'''

from pathlib import Path
from typing import Callable, Dict, Union, Iterable, Tuple, List
from attr import fields
from errno import ENOENT

from ruamel.yaml import YAML
from os.path import isfile

yaml = YAML(typ='safe')

def and_fns(*fns):
    return lambda x: all(map(lambda y: y(x), fns))

def in_exts(exts):
    return lambda file: file.suffix.lower() in exts

def first_file_with_ext(p_dir: Union[Path, str], name: str, exts: List[str]) -> Path:
    p_dir = Path(p_dir)
    _in_exts = in_exts(exts)
    is_name = lambda file: file.stem == name

    condition = and_fns(isfile, in_exts, is_name)
    files = filter(condition, p_dir.iterdir())
    
    try:
        file = next(files)
    except StopIteration:
        errmsg = f'File with name {name}.yml or {name}.yaml was not found'
        raise FileNotFoundError(errmsg)
    
    return file

def get_featured(p_dir: Union[Path, str]) -> Path:
    exts = ['jpeg', 'jpg', 'png', 'mp4']
    
    return first_file_with_ext(p_dir, 'featured', exts)

def try_yaml(p_dir: Union[Path, str], name: str) -> Dict:
    p_dir = Path(p_dir)
    maybe_file = first_file_with_ext(p_dir, name, ['yaml', 'yml'])
    text = maybe_file.read_text(encoding='utf-8')

    return yaml.load(text)

def map_dict(func: Callable, dictionary: Dict):
    '''
    Returns a dict with the same keys as `d` and with its
    values passed over `func`
    '''
    return dict(zip(dictionary.keys(), map(func, dictionary.values())))


def compat_dict(cls, dictionary):
    '''
    This serves a easy way to call the constructor of an attrs-decorated class.
    It removes any unnecesary key from the dictionary that wouldn't fit the
    constructor, it would fail otherwise.
    ''' 
    # cls should be an attrs-decorated class
    return dict((k.name, dictionary.get(k.name)) for k in fields(cls) if k.name in dictionary)