## ¿What is a Website Builder?

Simply speaking, is an online service for people who need create a website quickly and without any programming knowledge.

It is **ideal for those who want to start an online business without having to hire programmers or designers.**

<video autoplay muted loop> <source src="/assets/img/website_builder/wysiswyg.mp4" type="video/mp4"> <p>Your browser does not support the video tag.</p> </video>

The ease of use of website builders is due to its editing interface using an HTML editing software called WYSIWYG (What You See Is What You Get), which **allows you to edit and visualize the changes at the same time.**

Like the editing interface, there are also other aspects (which we'll talk about later) that make this service a great tool.

## Free Website Builder

Most of the Website Builders that exist in the current online market offer a free plan.

This basic plan usually has the following features.

![Free Website Builder Features](/assets/img/website_builder/free.jpg "Free Website Builder Features")

### Monthly Bandwidth

A site's bandwidth determines how much data your visitors can view over time.

All companies offer in their free version 2GB maximum bandwidth.

### Storage

Storage capacity is required in order to upload content to a website. The larger the storage space, the more photos, videos or text can be uploaded to a website.

All companies offer in their free version 500MB of storage at most.

### Website Builder Subdomain and Ads

All website builders add the subdomain with their company name in the address of the site to be created (for example: mynewsite.webtechindex.com) and also a watermark or ads from them.

There are some companies that in their free version allow to connect a separate domain (for example mynewsite.com) without having to use its subdomain.

## Factors to consider before choosing a Website Builder

Because there are many companies that offer the Website Builder service, we offer you a list of factors that are important when choosing which to stay with.

![Factors to consider before choosing a website builder](/assets/img/website_builder/factors.jpg "Factors to consider before choosing a website builder")

### Design and Customization

In today's competitive and online environment, it is important to have a website that looks professional and stylish. That's why **website builders strive to offer their customers a wide range of themes and quality templates**, aimed at various business areas.

A good website builder should provide a large number of templates of the best quality and the ease of being able to modify them as many times as you want, to suit the type of website you need.

### Domain

Domain is important for people who want to have a good business image. Therefore a company will have a bad image if it has the name of its domain like mynewbusiness.webtechindex.com.

The variety of TLDs (ccTLDs and gTLDs) is of great importance when it comes to choosing a website builder, because **having a wider range of options, the better it can be the web domain** for the new online business.

Generally, website builders offer the free domain when paying the annual subscription to the service, but the right to maintain the domain must be paid separately each year. **Is important to corroborate which Domain Registrar they work with and what is the annual renewal fee for a web domain depending on the TLD.**

### Mobile Responsive

It is very important that the pages created by the website builder are responsive to mobile, because today the vast majority of online users use these devices.

A good website builder offers the option to change the display of its editing interface in order to pre-visualize the website on a mobile, tablet or computer.

### Google Friendly

When you create a website and make it available to everyone, you often ask yourself: How can I get people to join my website?

There are many ways for people to find a web page on the internet, **the one that generates the most traffic is the organic search** or in other words, that people come in by finding the site in the Google search engine.

That's why website builders should be concerned with giving good SEO tools (Search Engine Optimization) so that **users can rank in Google's search engine.**

### Social Integration

A good website builder must have the ability to connect to social networks, so visitors can share or comment from their Facebook, Twitter and Google+ accounts.

## 4 Steps to make your Website

With a website builder, making your own page will be a very easy job. Just follow these simple steps.

![Steps to make a website](/assets/img/website_builder/steps.jpg "Steps to make a website")

### Choose a Template

The first thing to do is to choose the ideal template for the type of business you are going to launch. This may take a while because of the large number of templates that website builder offer.

In case the website builder gives you the option to choose color theme, also do it according to your needs.

### Customize your Website

Use the website builder's editing interface to sort the structure of your site, also to create the pages that are necessary (for example: contact, blog, prices, etc.).

Once you have made the global structure of the site, order how you are going to put your content on each page. You can also change the font, add images, videos and even add icons.

Have the information and structure of the page ready before publishing it. By the way, there is nothing wrong with making a mistake in the content or structure of your site, because after launching your site you can make more changes.

### Select your Domain

It is very important that you take this step calmly, because once you choose a domain name you will not be able to change it (only paying about 10 dollars more).

First choose the Top Level Domain (TLD) for your site, for example: .com, .net or .org.

Then you must have a name thought for your website (for example webtechindex) and you **should verify that the name next to the TLD you have chosen are available.** If they are available, good for you; if not, you must change the site name or the TLD, or both.

[Click here if you have any questions about domain names.](webtechindex.com/domain_name)

### Launch to the Internet

With the whole site ready you only have to click on the Publish button and you will have your web page.

Do not forget that you can make the changes you want after you have published your website.
