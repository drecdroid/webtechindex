## ¿What is a Domain Name?

A domain name is a mean to access to your website without having to remember your internet address.

Because your website is stored in some part of the world it could be in Vancouver, New York, Amsterdam, or even distributed across many servers at the same time; it is assigned an address to be identified and accessed.

The address of your website is provided in one of two standard formats, IPv4 or IPv6, these are the current standards, being all IPv4 addresses in transition to become IPv6.

![How an internet address looks](/assets/img/domain_name/formats.png)

Imagine your users having to remember all those numbers every time they want to access to your site. That is when domain names are necessary. Domain names maps in some way(later explained), to your server's address.

## Selecting a Domain Name

Your domain name is composed of two parts. The first is the name or second level domain, the second part is the extension or top level domain(TLD). So in mybusiness.com, "mybusiness" is the name and "com" is the TLD.

Depending on your needs you will select a different TLD

The only thing that stops you from acquiring a domain name is the availability of it. They could be registered by someone else at the moment.



## Registering a Domain Name

Once you have selected your domain name you have to register it. In order to do this you will need a domain name registrar, an accredited entity capable of manage a registry.

The are plenty of registrar options available, as business as they are, they will offer different prices and features before, during and after your registration. But be careful, once you selected your registrar you are tied to them unless you request a registrar transfer.

Depending on your selected registrar the process will have some differences, but almost them all have these steps:

- You write your wanted domain name in a box.
- You select it if available or a similar one from a list.
- The domain is added to a cart.
- You select how many years you want the domain to be registered(1 to 10 years)
- You select additional services/features.
- You pay for the domains on your cart.

After this you can be considered the owner of the domain and only have to remember to renew your domain when it is about to expire.

## Buying a Parked Domain Name

It could be the case that the domain you want is already taken but is not really being used. Instead if you access the website you will see a generic page with only links. These kind of domains are called parked domains, and they are solely bought to be sold at an higher price in the future.

The owners of parked domains only pay for annual renewal, so in some cases like in .com domains they only have to pay about 9 dollar yearly, and they sell these domains for hundreds of dollars depending on how valuable they are.

## Protecting Your Privacy

When you register a domain you have to provide you personal information, and unfortunately this information will be stored in a public registry. Anyone could get it executing a WHOIS request via command line or a web service.

This opens the gate for malicious intentions like spamming, marketing, identity thieves and other kinds of attacks.

Probably you'll want to protect your information. Because of that registrar provides means to mantain your information secure.

There are two ways in that your WHOIS information can be cloaked.

- The registrar cloaks the information with some dummy information.
- The registrar owns your domain and you'll be able to use it.



## Transferring a Domain Name


## Binding your domain name with your website

When someone types a domain name and tries to access it, the browser will send a request to a nameserver looking in there for a information about this domain, the most common information is the correspondent IP address for this domain name.

If your domain name is not found in the local DNS server, it will look in the ISP's nameserver, and so on until the information about your domain is found in some superior nameserver.

You will want to change this information about your domain name to point it to another host, to create multiple subdomains, to point a mail server or multiple other things(remember, your domain name is just an alias to other addresses). To change the information you will have this trough the nameserver provided by your registrar.

This information consist of many textual records or directives that just say how to answer to certain domain requests. If you change some of this information, the nameserver will propagate your changes to every major nameserver out ther.

It will take some time to propagate your changes to every nameserver out there, and you'll probably not see your changes in your browser right away if you accessed before to your site because your local nameserver has saved information about your domain from previous request. You will need to clean your nameserver's cache in order to probably see the changes from your registrar on the internet.

This is probably the most difficult part about domain name handling, but is also the most powerful. It gives you the possibility to point to different services, to have multiple sites in one domain, to custom information for other services, to connect to a mail server, a ftp server, to any other kind of server.

## New gTLDs List

|               |                |              |              |
| ------------- | :------------: | -----------: | -----------: |
| .adult        | .agency        | .airforce    | .alsace      |
| .apartments   | .archi         | .army        | .art         |
| .associates   | .attorney      | .auction     | .audio       |
| .auto         | .baby          | .band        | .bar         |
| .bargains     | .bayern        | .beer        | .berlin      |
| .best         | .bet           | .bid         | .bike        |
| .bingo        | .bio           | .black       | .blackfriday |
| .blog         | .blue          | .boutique    | .brussels    |
| .build        | .builders      | .business    | .buzz        |
| .bzh          | .cab           | .cafe        | .cam         |
| .camera       | .camp          | .capetown    | .capital     |
| .car          | .cards         | .care        | .career      |
| .careers      | .cars          | .casa        | .cash        |
| .casino       | .catering      | .center      | .ceo         |
| .chat         | .cheap         | .christmas   | .church      |
| .city         | .claims        | .cleaning    | .click       |
| .clinic       | .clothing      | .cloud       | .club        |
| .coach        | .codes         | .coffee      | .college     |
| .cologne      | .community     | .company     | .computer    |
| .condos       | .construction  | .consulting  | .contractors |
| .cooking      | .cool          | .country     | .coupons     |
| .courses      | .credit        | .cricket     | .cruises     |
| .cymru        | .dance         | .date        | .dating      |
| .deals        | .degree        | .delivery    | .democrat    |
| .dental       | .dentist       | .desi        | .design      |
| .diamonds     | .diet          | .digital     | .direct      |
| .directory    | .discount      | .doctor      | .dog         |
| .domains      | .download      | .durban      | .earth       |
| .eco          | .education     | .email       | .energy      |
| .engineer     | .engineering   | .enterprises | .equipment   |
| .estate       | .eus           | .events      | .exchange    |
| .expert       | .exposed       | .express     | .fail        |
| .faith        | .family        | .fans        | .farm        |
| .fashion      | .film          | .finance     | .financial   |
| .fish         | .fishing       | .fit         | .fitness     |
| .flights      | .florist       | .flowers     | .football    |
| .forsale      | .foundation    | .frl         | .fun         |
| .fund         | .furniture     | .futbol      | .fyi         |
| .gal          | .gallery       | .games       | .garden      |
| .gent         | .gift          | .gifts       | .gives       |
| .glass        | .global        | .gold        | .golf        |
| .graphics     | .gratis        | .green       | .gripe       |
| .group        | .guide         | .guitars     | .guru        |
| .hamburg      | .haus          | .healthcare  | .help        |
| .hiphop       | .hiv           | .hockey      | .holdings    |
| .holiday      | .horse         | .hospital    | .host        |
| .hosting      | .house         | .how         | .immo        |
| .immobilien   | .industries    | .ink         | .institute   |
| .insure       | .international | .investments | .jetzt       |
| .jewelry      | .joburg        | .juegos      | .kaufen      |
| .kim          | .kitchen       | .kiwi        | .koeln       |
| .kyoto        | .land          | .lat         | .law         |
| .lawyer       | .lease         | .legal       | .lgbt        |
| .life         | .lighting      | .limited     | .limo        |
| .link         | .live          | .loan        | .loans       |
| .lol          | .london        | .love        | .ltd         |
| .ltda         | .maison        | .management  | .market      |
| .marketing    | .mba           | .media       | .melbourne   |
| .memorial     | .men           | .menu        | .miami       |
| .moda         | .moe           | .mom         | .money       |
| .mortgage     | .moscow        | .nagoya      | .navy        |
| .network      | .news          | .ngo         | .ninja       |
| .nrw          | .nyc           | .okinawa     | .one         |
| .onl          | .online        | .organic     | .osaka       |
| .paris        | .partners      | .parts       | .party       |
| .pet          | .photo         | .photography | .photos      |
| .physio       | .pics          | .pictures    | .pink        |
| .pizza        | .place         | .plumbing    | .plus        |
| .poker        | .porn          | .press       | .productions |
| .promo        | .properties    | .property    | .pub         |
| .qpon         | .quebec        | .racing      | .recipes     |
| .red          | .rehab         | .reise       | .reisen      |
| .rent         | .rentals       | .repair      | .report      |
| .republican   | .rest          | .restaurant  | .review      |
| .reviews      | .rich          | .rip         | .rocks       |
| .rodeo        | .ruhr          | .run         | .ryukyu      |
| .saarland     | .sale          | .salon       | .sarl        |
| .school       | .schule        | .science     | .scot        |
| .security     | .services      | .sexy        | .shiksha     |
| .shoes        | .shop          | .shopping    | .show        |
| .singles      | .site          | .ski         | .soccer      |
| .social       | .software      | .solar       | .solutions   |
| .soy          | .space         | .store       | .stream      |
| .studio       | .study         | .style       | .supplies    |
| .supply       | .support       | .surf        | .surgery     |
| .sydney       | .systems       | .taipei      | .tattoo      |
| .tax          | .taxi          | .team        | .tech        |
| .technology   | .tennis        | .theater     | .theatre     |
| .tienda       | .tips          | .tires       | .tirol       |
| .today        | .tokyo         | .tools       | .top         |
| .tours        | .town          | .toys        | .trade       |
| .trading      | .training      | .tube        | .university  |
| .uno          | .vacations     | .vegas       | .ventures    |
| .versicherung | .vet           | .viajes      | .video       |
| .villas       | .vin           | .vip         | .vision      |
| .vlaanderen   | .vodka         | .vote        | .voting      |
| .voto         | .voyage        | .wales       | .wang        |
| .watch        | .webcam        | .website     | .wed         |
| .wedding      | .whoswho       | .wien        | .wiki        |
| .win          | .wine          | .work        | .works       |
| .world        | .wtf           | .在线        | .移动        |
| .онлайн       | .сайт          | .орг         | .中文网      |
| .संगठन        | .机构          | .みんな         | .游戏        |
| .企业         | .xyz           | .yoga        | .yokohama    |
|.zone	|||

