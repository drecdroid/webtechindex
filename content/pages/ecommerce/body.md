## What is e-Commerce?

El e-Commerce o también conocido como comercio electrónico, online o virtual es un método de compra-venta de bienes, productos o servicios ya sean material o digital tramitadas por internet.

Este tipo de comercio se ha convertido muy popular debido al auge del internet, la facilidad de difusión por redes sociales y el creciente interés de las personas en comprar por internet, ya sea usando laptop o celular.

## e-Commerce types

Existen 6 tipos de comercio electrónico que dependen los participantes en la acción de compra-venta online.

### B2B 

**Business-to-Business:** Comercio electrónico en donde las operaciones comerciales se dan entre empresas que operan en internet y no con consumidores.

El objetivo principal de la relación entre empresas que existe en este tipo de comercio electrónico, es poder generar un bien o servicio digital para el consumidor.

Es necesario tener experiencia en el mercado online para poder participar en este tipo de comercio electrónico.

### B2E 

**Business-to-Employee:** Esta forma de comercio electrónico se da principalmente como su mismo nombre lo dice, entre una empresa y sus empleados.

En otras palabras, son las ofertas que la misma empresa puede ofrecer a sus empleados mediante su tienda online.

Este tipo de comercio electrónico se ha puesto de moda en las empresas para generar un mejor clima laboral y competencia entre los empleados.

### B2C

**Business-to-Consumer:** Es la forma de comercio electrónico más conocida, en donde las operaciones comerciales entre un negocio (tienda virtual) y una persona interesada en comprar uno de sus productos o servicios.

En este tipo de comercio electrónico es muy importante la participación de los e-Commerce platforms, que es la plataforma virtual donde el negocio pone a disposición sus productos o servicios y las personas pueden verlos desde sus laptops, tablets o celulares.

### C2C

**Consumer-to-Consumer:** Es la forma de comercio electrónico que se da entre personas naturales.

Cuando una persona ofrece algún producto, ya sea porque no lo utiliza o que lo haya fabricado, a otras personas mediante plataformas o tiendas virtuales para realizar la transacción.

Esto funciona similar a las conocidas "ventas de garaje" con la única diferencia que se da por internet.

### G2C

**Government-to-Consumer:** Es la forma de comercio electrónico que se da cuando un gobierno municipal, estatal o federal permita que las personas realicen sus trámites a través de su portal online.

Se considera parte de los tipos de comercio electrónico porque se efectúa un pago de trámite por internet.

### C2B

**Consumer-to-Business:** Es una forma de comercio electrónico poco convencional en la que personas venden sus productos o servicios a empresas u organizaciones.

Un claro ejemplo de esto son los freelancers que venden sus servicios particulares a empresas cuando estas lo requieran.

https://marketingdigitaldesdecero.com/2013/05/27/introduccion-al-comercio-electronico-e-commerce/

## Advantajes of e-Commerce

Las ventajas están dirigidas tanto para los ofertantes como para el consumidor. 

Por ese motivo explicaremos algunas ventajas que trae consigo el comercio electrónico.

### Para los ofertantes:

+ **Mejor comunicación:** El comercio electrónico permite que los ofertantes puedan dar a conocer sus productos o servicios por medio de sus portales, tiendas online, marketplaces o por alguna red social.

También ayuda a una mejor comunicación entre el ofertante y el consumidor en el momento de la transacción de compra, porque pueden comunicarse vía e-mail o chat de soporte al cliente.

+ **Mejor distribución:** Cuando se venden productos o servicios digitales, el ofertante no deberá preocuparse por nada de logística, porque no existe el envío material de algo.

+ **Fidelización de clientes más rápido:** Mediante la aplicación de estrategias de marketing y de comunicación efectiva mediante diversos canales de tráfico (como Facebook), van a permitir que las personas puedan enterarse de la compañía e ir viendo como esta crece durante el tiempo.

+ **Reduce costos:** Abrir una tienda online es mucho más barato que pagar la renta de un local, servicios, sueldos, etc..

Además, la publicidad online es más económica que la publicidad en medios de comunicación tradicionales.

### Para los consumidores:

+ **Comodidad al comprar:** El comprador puede ver todos los productos o servicios que quiera desde donde quiera y cuando quiera, desde la su celular, laptop o tablet, sin necesidad de trasladarse de tienda en tienda.

+ **Encontrar productos a menor costo:** La gran competencia que existe en el mercado digital favorece a la reducción de precios en los productos y en la preocupación de ofrecer la mejor calidad.

Por ende, los clientes van a poder encontrar productos y servicios de buena calidad y a un precio exacto.

+ **Privacidad y Seguridad:** El consumidor puede efectuar su compra con la máxima intimidad y con la máxima seguridad.

Es importante verificar que la tienda online ofrece distintas medidas de seguridad al momento de efectuar una compra.

## Must common known e-Commerce terms

Existen términos que son importantes conocer para poder desenvolverse exitosamente en el mundo del e-Commerce.

+ **ERP: (Entrerprise Resouce Planning)** Es un conjunto de sistemas de información gerencial que ayuda a la integración de ciertas operaciones de una empresa, especialmente las que están relacionadas con la producción, la logística y la contabilidad.

+ **CRM: (Customer Relationship Management)** Es una nueva forma de gestionar las relaciones con los clientes, el cual es el resultado de combinar las antiguas técnicas comerciales con la tecnología de la información.

+ **SCM: (Supply Chain Management)** Es un proceso enfocado a optimizar la planeación, puesta en ejecución y control de las operaciones de la cadena de suministro de una empresa con el propósito de satisfacer las necesidades del cliente con toda la eficacia posible.

+ **BI: (Busines Intelligence)** Uso de estrategias y herramientas para el análisis de datos de una empresa para facilitar la toma de decisiones.

+ **SWOT: (Strengths-Weaknesses-Opportunities-Threats)** Es una herramienta de planificación estratégica, diseñada para realizar una análisis interno (Fortalezas y Debilidades) y externo (Oportunidades y Amenazas) en la empresa.

+ **Market Analysis:** Es la recopilación y análisis de información para hacerse idea sobre la viabilidad comercial de una actividad económica. 

+ **Brick & Mortar:** Organización que tiene participación comercial de forma tradicional, es decir, con instalaciones físicas. 

+ **Dot-com Company:** Organización que hace todas sus operaciones por internet, generalmente a través de un website con un TLD ".com".

+ **Bricks and Clicks:** Organización que tiene participación offline (Brick & Mortar) y online (Dot-com Company).

## How to sell online?

Si quieres iniciar un negocio de venta de productos o servicios, si tienes algún objeto que ya no usas y que lo deseas vender, pues debes saber que existen muchas maneras de vender por internet.

A continuación, te enseñaremos algunas de estas:

### Online Marketplace

Los mercados en línea ofrecen el tipo de e-commerce C2C y tabmién B2C. En estas plataformas online de compra y venta, los usuarios tomar la posición de vendedor poniendo a disposición de venta o subasta sus productos o servicios, y también pueden tomar una posición de comprador.

Lista de Online Marketplaces:

+ [eBay](https://www.ebay.com/)
+ [Amazon](https://www.amazon.com)
+ [Alibaba](https://www.alibaba.com/)
+ [Etsy](https://www.etsy.com/)
+ [Bonanza](http://www.bonanza.com/)
+ [Zazzle](https://www.zazzle.com)

### Apps to Buy and Sell Stuff

Si necesitas vender algún artículo de manera rápida y sencilla, existen aplicaciones para smartphones basadas en la ubicación que sirven para hacer ventas locales con personas que viven cerca de ti.

Lista de Apps de este tipo:

+ Letgo - [Android](https://play.google.com/store/apps/details?id=com.abtnprojects.ambatana&hl=en) / [iOS](https://itunes.apple.com/us/app/letgo-buy-sell-second-hand-stuff/id986339882?mt=8)
+ Wish Local - [Android](https://play.google.com/store/apps/details?id=com.contextlogic.wishlocal&hl=en) / [iOS](https://itunes.apple.com/us/app/wish-local-buy-sell/id1035843887?mt=8)
+ Linger - [Android](https://play.google.com/store/apps/details?id=com.app.localinger&hl=en)
+ Carousel - [Android](https://play.google.com/store/apps/details?id=com.thecarousell.Carousell&hl=en) / [iOS](https://itunes.apple.com/us/app/carousell/id548607187?mt=8)
+ 5miles - [Android](https://play.google.com/store/apps/details?id=com.thirdrock.fivemiles&hl=en) / [iOS](https://itunes.apple.com/us/app/5miles-buy-and-sell-locally/id917554930?mt=8)

### Sell on Social Media

Las redes sociales hoy en día tienen un gran protagonismo en el día a día de las personas alrededor del mundo, por lo que lo hace un gran canal de posibles comprantes.

Existen muchas redes sociales y también estrategias para poder ofrecer tus productos, el éxito está en saber encontrar en cual de estas puedes vender mejor tus productos o servicios.

Lista de Redes Sociales en la que puedes vender:

+ [Facebook Marketplace](https://www.facebook.com/marketplace)
+ [Instagram](https://www.instagram.com/)
+ [Pinterest](https://www.pinterest.com/)

http://www.finanzaspersonales.co/trabajo-y-educacion/articulo/10-tips-para-vender-redes-sociales/54649
https://marketingland.com/sell-social-media-200201

### Starting an Online Store

En el caso de que quieras iniciarte en el mundo del e-commerce, lo mejor para ti sería crear un Online Store.

En donde podrás tener un sitio web con el nombre de tu marca, personalizarlo como tu quieras y gestionar los productos, servicios, envíos, etc..

Existen muchos e-Commerce website builders hoy en día, todos apuntando a un mismo objetivo, ayudar al pequeño empresario a poder salir adelante.

Lista de e-Commerce Website Builders / Online Store Builders:

+ [3dcart](https://www.3dcart.com/)
+ [Shopify](https://www.shopify.com/)
+ [BigCommerce](https://www.bigcommerce.com/)
+ [WixStores](https://www.wix.com/ecommerce/website)

## What is an e-Commerce Platform?

Como mencionamos anteriormente, las e-Commerce Platforms o también llamadas Online Store Builder son un buen inicio para emprender un negocio de venta de productos o servicios por internet.

Y que vienen a ser estos e-Commerce Platforms?

Bueno, son aplicaciones de software que ayudan a las personas y empresas a poder crear y gestionar una página web con tienda online incluida.

Además, gracias a este servicio online, se puede gestionar un negocio (tienda) en línea con potentes herramientas y funciones, permitiendo administrar y planear las operaciones.

## 6 Steps to Starting an Online Store?

Si te preguntabas ¿y ahora cómo empiezo una tienda online?, pues sigue estos 6 simples pasos para que tengas noción de como usar un e-Commerce platform.

### Choose and Sign Up to a e-Commerce Platform

Primero deberás escoger que e-Commerce platform vas a usar para hacer tu negocio. Debes saber que existen muchos y por eso sería recomendable que leas  los factores a considerar antes de escoger un e-Commerce website builder.

Una vez que escogiste una de las compañías que ofrecen este servicio vas a tener que registrarte en la versión gratuita o de paga, depende de las necesidades de tu negocio.

Lo recomendable es primero registrarse en la versión gratuita y probarlo, si sientes que se acomoda a tus necesidades y te gusta su interfaz de edición, pues recién ahí piensa en que plan de pago se acomoda a tu negocio y a tu bolsillo.

### Create and Customize your Store

Crea tu tienda online y escoge una de las plantillas o temas que te provee el online store builder.

Personaliza la apariencia de tu tienda con la que representes lo que vas a vender y al público al que va dirigido tu negocio.

Ordena la estructura de su tienda usando la interfaz de edición, también cree las páginas (contacto, acerca de mi, etc.) y blog para poder tener mayor contacto con sus futuros clientes.

### Upload Content and Products

With e-Commerce platforms you can easily upload your products, texts, images in just minutes.

Controla la información de los productos como la descripción, precios, peso, título, entre otras.

Si vas a vender productos digitales, deberás subir estos a tu tienda para que las personas que los compren puedan descargarlos.

### Setup Store Details

Para hacer este paso deberás conocer del tema o leer textos que te capaciten en el comercio online.

Deberás administrar los métodos de pago, métodos de envío, tax rates, store default currency, track inventory, etc..

Generalmente los e-commerce website builders ofrecen guías detalladas para que puedas hacer estos cambios con éxito.

### Launch your Online Store

Una vez que ya tienes tu tienda con la información necesaria y con los productos bien descritos es momento de que lances tu tienda online a todos los usuarios del mundo.

Utiliza las herramientas SEO y de marketing que te ofrece tu e-Commerce Platform, para que así puedas hacer que las personas conozcan tu negocio.

Ofrece en tu tienda newsletters, promotions, coupons, reward points para tener una mejor aceptación de tu público.

### Manage

Gestiona las ordenes rápida y efectivamente, como también tu inventario y pedidos a proveedores, usando el sistema de gestión de tu e-Commerce platform.

Utiliza las herramientas de análisis para ver el progreso de tu tienda online y poder hacer una planeación de la producción y de inventarios.

## Factors to Consider Before Choosing a e-Commerce Platform

Debido a que existen muchas compañías que ofrecen este servicio y es difícil escoger entre tantas opciones, les ofrecemos esta lista de 7 aspectos que debes considerar al momento de escoger un Online Store Builder.

### Templates and Design Flexibility

Todos los e-Commerce platforms ofrecen plantillas y temas para que la creación rápida de un sitio web con buen aspecto, para que así no tengas que estar preocupándote en hacer todo el diseño y estructura de tu sitio web desde cero.

Pero debes procurar que el e-Commerce website builder que escojas tenga muchas plantillas y que se ajusten al tipo de negocio que vas a lanzar.

Aparte de las plantillas, debes verificar que la interfaz de edición tenga todas las herramientas necesarias para poder modificar el diseño de tu tienda como quieras y cuantas veces quieras.

### Social Media Integration

Las redes sociales están teniendo mayor importancia hoy en día.

Debido a eso, es muy importante la integración de tu tienda online con estas social media platforms.

Piensa, si tan solo Facebook tiene un tráfico de más de 2 millones de personas activas al mes, imagínate poder captar a un pequeño porcentaje de estos hacia tu negocio y convertirlos en compradores. Eso sería el éxito.



https://www.aawebmasters.com/choosing-ecommerce-software/
https://www.bigcommerce.com/ecommerce-answers/what-ecommerce-platform/

### Quantity Limitation of Products

https://www.bigcommerce.com/ecommerce-answers/what-ecommerce-platform/

### Payment Gateways

https://www.aawebmasters.com/choosing-ecommerce-software/

### Marketing

https://www.aawebmasters.com/choosing-ecommerce-software/

### Security in Payment

https://www.aawebmasters.com/choosing-ecommerce-software/

### Customer Support

https://www.bigcommerce.com/ecommerce-answers/what-ecommerce-platform/

e-Commerce store / e-commerce platform / online store


## Successful Online Store Pillars

http://timetoduckandcover.com/successful-online-store.html

https://marketingdigitaldesdecero.com/2013/05/29/los-5-pilares-de-una-tienda-online-e-commerce/

## Online Store basic elements

https://marketingdigitaldesdecero.com/2013/06/06/elementos-basicos-de-una-tienda-virtual/

## How promote your digital business?

https://www.entrepreneur.com/article/268503