## Overview

[Duda](https://www.duda.co) is a website builder with more than seven years
in the market and more than 9 million hosted websites.

They focus on improving the presentation and functionality of web pages made
for  mobile devices.

>The Duda website has been created with the same platform to make responsive
pages.


Duda's services are aimed at all kinds of people, from beginners in design and
programming to web professionals.

> Duda is the only Google-preferred website builder that optimizes each
> and every site built on our platform for Google PageSpeed.


This is a strong claim about their SEO performance and we will try to prove
this in the future.

Below, we'll look at some of its features in more detail.


## Ease of Use

As most, if not all, website builders, Duda offers to users a drag-and-drop
editing interface, with which one can modify the selected template with ease
and in any way you want.

<video autoplay muted loop> <source
src="/assets/img/duda_ease_to_use.mp4" type="video/mp4">
<p>Your browser does not support the video tag.</p>
</video>


A Developer Mode is also available for those with enough experience to handle this
task, they would be able to customize the HTML and CSS of the template.

You can also preview and edit your website as it would be in other devices
like desktop, tablet and mobile.

See how it works:


<iframe width="560" height="315"
src="https://www.youtube.com/embed/PAbHqo7UC9E" frameborder="0"
allowfullscreen></iframe>


## Templates

Duda provides almost 100 templates for different needs, all of which are
responsive, professional quality and minimalist. 


![Duda template interface](/assets/img/duda_templates.jpg "Duda template
interface")


One drawback is that once you choose your template, you can't change it for
another one and you'll need to reset your website, if you want use other
template.

After choosing one, you'll be able to customize the template without
any limits.

## SEO 

Duda gives you the posibilitie to enable easy SEO, with basic SEO features,
like:

+ Set page description + Alt tags + Auto generated sitemaps + Mobile-friendly
design (mobile SEO) + Meta keywords and description + rel="canonical" Tags
(mobile SEO)

Duda also provide a [SEO
guide](https://help.duda.co/hc/en-us/articles/219105808) with all the
information you need for ranking in Google search engine

But, the most important feature for SEO is the PageSpeed score for all the
websites created by Duda.

You may wonder, why speed is a ranking factor for Google?

Here's the reason:

![Duda Speed rank factor for Google](/assets/img/duda_speed_seo.jpg "Duda
Speed rank factor for Google")

Duda is the only Google Preferred Website Builder that is fully aligned with
the search giant’s best practices for creating websites with the best possible
load times.

Every new Duda website created on the platform will now adhere to Google’s
recommended best practices for a great PageSpeed score.


## Website Personalization

Duda offers Business + users the InSite customization feature that beat other
website builders.

With this feature you can add and create special events(e.g. a pop-up) that
appear on your website if a previously established condition occurs. E.g.:
when a visitor is nearby from your restaurant, when a viewer a first-time
visitor or returning visitor and many others conditions.

![Duda personalization option](/assets/img/duda_personalization.jpg "Duda
personalization option")


## Resource Center

Duda gives you free access to its resource center where you can read all the
information they provide, like [case
studies](https://resources.duda.co/case-study), [product
overview](https://resources.duda.co/product-overview) and [sales
collateral](http://resources.duda.co/sales-collateral) material (only for
DudaPro users).

Duda also provides [free webinars](https://resources.duda.co/webinars) for
increase the performance of your website.

![Duda resource center](/assets/img/duda_resource_center.jpg "Duda resource
center")


## e-Commerce

This website builder comes with a built-in e-Commerce solution that display
perfectly and works seamlessly across desktop, tablet and mobile.


<video autoplay loop> <source
src="/assets/img/duda_ecommerce.mp4" type="video/mp4"> Your browser does not
support the video tag. </video>


Some of the features Duda gives you for your online store are:

+ Email
+ Generate coupons
+ Shipping and tax options
+ Tracking codes
+ Mobile store management app (Android and iOS)
+ SEO for your products
+ Wide range of payment options

![Duda e-Commerce features](/assets/img/duda_ecommerce.png "Duda e-Commerce
features")

If you want all the features with more detail, [click
here](https://www.duda.co/plan/ecommerce-features-full)


## Plans

Duda offers 3 products, all for create a website:

### Responsive Websites

+ Free

+ Business+: Yearly US$14.25/month - Monthly US$19.00/month

+ Business + e-Commerce (100 products): Yearly US$22.50/month - Monthly US$29.00/month

+ Business + e-Commerce (2500 products): Yearly US$36.75/month - Monthly US$46/month


[More information](https://www.duda.co/plan/d1)

### Mobile-only Websites

+ Free

+ Premium: Yearly US$7.20/month - Monthly US$9.99/month


[More information](https://www.duda.co/plan/dm)

### Reseller Plan

+ DudaPro: US$249/year


[More information](https://www.duda.co/plan/pro)
