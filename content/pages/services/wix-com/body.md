## Overview

[Wix.com](https://www.wix.com/) is ​​a website builder with millions of users
around the world and years in the web market.

Wix offers its users tools for building a website easily and to increase
performance in SEO (Search Engine Optimization) as well as a wide range of
templates and applications in their App Market.

Below, we'll look at some of its features in more detail.


## Templates Variety

**It's not only the amount of more than 500 templates available to users,
because it also amazes the professional quality of these.**

> Wix cares alot about its templates, so it's one of its qualities with which
it differs from other website builders.


You can also make your own template with a blank canvas, which only have the
structure of the site, but not the detailed design.

![Wix template interface](/assets/img/wix_template.jpg "Wix templates
interface")

Obviously, we recommend you that use their pre-made templates and make the
modifications you want, saving your time.


**Wix sorts the templates with respect to the purpose or niche of the users,
so that you can find more easily the template that suits your needs.**


## Wix ADI - ARTIFICIAL DESIGN INTELLIGENCE

Wix created an artificial intelligence capable of making the first version of
your website with respect to the needs you indicate. With this feature Wix
differs greatly from its competitors.

![Wix Artificial Design Intelligence interface](/assets/img/wix_adi.jpg "Wix
Artificial Design Intelligence interface")

After Wix ADI makes the initial version of your website, it helps you step by
step to be able to make modifications like change the website design, colors,
animations, fonts, among others. Finally, you will get your custom website in
minutes.

>ADI is an excellent sign that Wix is ​​in a system of continuous innovation
to improve the experience of its users.


Take a look at this video and you’ll see how Wix ADI works:

<iframe width="560"
height="315"src="https://www.youtube.com/embed/YrERmaDZNl0" frameborder="0"
allowfullscreen></iframe>



## Wix SEO 

When it comes to Search Engine Optimization, [Wix SEO
Wiz](https://www.wix.com/about/wix-seo) will facilitate to get the best Google
ranking for your website.

<video width="800" height="522" controls> <source
src="/assets/img/wix_mobile.mp4" type="video/mp4"> Your browser does not
support the video tag. </video>

This awesome feature has what you need to posisionate your website and get
more traffic, including a personalized plan, keyword analyzer, step-by-step
walktroughs and a very useful [SEO Guide](https://seoguide.wix.com/en/).

Google values great user experience when determining search results, and sites
that are mobile optimized rank better when searching from your phone or mobile
device. For this reason, **Wix gives you the option to optimize the mobile
version of your website to look great on any device.**


## App Market

Another aspect that fascinated us, was the amount of applications they offer
in their app store. **There are more than 250 apps that you can integrate to
your website**, many of these are only for Premium users.

![Wix App Market interface](/assets/img/wix_app.jpg "Wix App Market
interface")

These applications will help you increase the performance of your website,
whether in SEO, finance and communication between workers. It also increases
the functions of your site, such as booking, testimonials, live chat whit
customers, email marketing and many more.

Click here to see [all available
applications](https://www.wix.com/app-market/main).


## Plans

Wix gives you 6 options with different features from US$0.00 to US$30.00 per
month.


+ Free

+ Connect Domain: Yearly US$4.50/month - Monthly US$7.00/month

+ Combo: Yearly US$8.50/month - Monthly US$12.00/month

+ Unlimited: Yearly US$12.50/month - Monthly US$16.00/month

+ eCommerce: Yearly US$16.50/month - Monthly US$20.00/month

+ VIP: Yearly US$24.50/month - Monthly US$30.00/month


Free plan include:


+ Free Wix Domain

+ Wix Brand Ads

+ Free Hosting

+ 500 MB Storage

+ 1 GB Bandwidth

All Premium Plans always include:

+ Free Hosting (only yearly subscription)

+ Domain Connection

+ 500 MB+ Storage

+ Google Analytics

+ Premium Support

+ No Set-up Fee


![Wix premium plans](/assets/img/wix_plans.jpg "Wix premium plans")

Click here to see [detailed plans
information](http://www.wix.com/upgrade/premium-plans).
