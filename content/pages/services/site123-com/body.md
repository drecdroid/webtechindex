## Overview

[Site123](https://www.site123.com/) is a new, simple and free website builder
with more than 250K websites built. 

> With Site123 you don’t need any design or coding experience and you can
build websites without hiring a professional.


This website builder is non-drag and drop like many others in the market, but
**have an advanced wizard with ready-made styles and layouts will teach you
how to make a website** and help you set up perfect online presence with
practically no effort.

You only have to worry about the content, Site123 will take care of all the
rest, helping you with its awesome features for free and premium users.

Below, we'll look at some of its features in more detail.


## Ease to use

First of all, is easy to register because you can sign up with your Google+ or
Facebook account, also you can use your email account.

<video autoplay loop> <source src="/assets/img/site123_ease_to_use.mp4"
type="video/mp4"> Your browser does not support the video tag. </video>

The advanced wizard is very simple and intuitive with useful options and tools
with which you can edit the style of your website in just few minutes.


## Site123 Design

**Is very easy to manage your content whit Site123 wizard.** You are able to
change the information of your entire website, add pages and position them on
the site you want, use the layouts and styles for give a different aparience
to your website.

To tailor your site to screen size of all devices (desktop, laptop, tablet,
cell phone), **Site123 give you responsive website layouts making your content
look perfect through any device.**

Also you can preview your site on desktop, tablet and mobile 

**At Site123 you have the option of choosing a multi page site or a one page
website.** It means that you can have a website where all your information and
posts meet on just a one page website, or divide the content into several
different tabs, directing the customer to the information they want to access
on another page.

See this video to see how it works.

<iframe width="560" height="315"
src="https://www.youtube.com/embed/vC4ytIqvLF0" frameborder="0"
allowfullscreen></iframe>


## SEO 

With Site123 you will be able to perform the Search Engine Optimization of
your website, but **don't have a wide range of utilies for SEO.** You can only
add the title meta tag, description meta tag and keywords meta tag for your
homepage and other pages (if you have a multi page site).

![Site123 utilies for SEO](/assets/img/site123_seo.jpg "Site123 utilies for
SEO")

A good Site 123's feature for SEO ae the Webmaster Tools for improve your site
in search console of Google, Bing and Yandex.


## Plugins

With the plugins you can notably increase and add new functions to your
website.

**Site123 offers few third-party plugins, something we didn't like about this
website builder.**

![Site123 Plugins](/assets/img/site123_plugin.jpg "Site123 Plugins")


## e-Commerce

Use the online store builder to put together a great place for your visitors
(future customers) to see your products and make purchases. With an online
store, you will be a step closer to having more clients.

![Site123 Web Store](/assets/img/site123_ecommerce.jpg "Site123 Web Store")

You'll able to:


+ Manage your orders and products

+ Create and manage coupons

+ Adjust your store's design

+ Receive payments with Paypal

+ many more options.


See how to add a digital product to your website.

<iframe width="560" height="315"
src="https://www.youtube.com/embed/SHiR3gwJcRE" frameborder="0"
allowfullscreen></iframe>


## Plans

If you don't have an account and enter [Pricing
page](https://www.site123.com/pricing) you'll see 2 prices from US$0.00 to
US$9.80 monthly when paid annually.

![Site123 Pricing Page](/assets/img/site123_pricing.jpg "Site123 Pricing
Page")

However, if you register as a free user and then go to upgrade options,
Site123 will offer 4 plans from US $ 9.80 to US $ 27.80 per month when paid
annually.

In total there are 5 plans:


+ Free

+ Basic: US$9.80/month

+ Advanced: US$15.80/month

+ Proffesional: US$21.80/month

+ Gold: US$27.80/month


![Site123 Plans](/assets/img/site123_plans.jpg "Site123 Plans")

We recommend you first register for a free account and see which plan fits
better to your needs. At the same time you can test if Site123 meets your
requirements.
