from flask import Flask, send_from_directory, send_file

app = Flask(__name__)

@app.route('/')
def send_index():
    return send_from_directory('output', 'index.html')
@app.route('/favicon.ico')
def send_favicon():
    return ''

@app.route('/assets/<path:path>')
def send_asset(path):
    return send_from_directory('output/assets', path)

@app.route('/<path:path>')
def send_html(path : str):
    f = path if path.endswith('html') else '{}.html'.format(path)
    return send_from_directory('output', f)

if __name__ == "__main__":
    app.run(host='0.0.0.0')