# Taxonomías
Si se quiere incluir nuevas taxonomías se tiene que definir un archivo `taxonomies.yaml` declarando claves con el nombre
de la carpeta donde se encuentran los archivos yaml de la taxonomía creada.

    #archivo: taxonomies.yaml

    # se declara la taxonomía dándole el nombre a una nueva clave
    # lo que significa que deberás crear una carpeta con el mismo nombre
    categories:
      # `generate_pages` por defecto es true, e indica si se generarán páginas con cada item de la taxonomía 
      generate_pages: false
      
      # `root` por defecto es el mismo nombre de la taxonomía convertido a slug, es el nombre con el que
      # se agruparán en en url. Por defecto sería /categories/[item-slug]
      root: "categoria"

cada item de una taxonomía puede redefinir variables comúnes entre las cúales están:

  - meta_description
  - title
  - slug

# Páginas

Todas las páginas van dentro de la carpeta pages, puedes agrupar páginas dentro de otras carpetas lo cual hará
que esta carpeta sea el root de la url. Por ejemplo si se colocan las páginas dentro de una carpeta `services`,
la url de cada página empezaría por  `/services/`. Se puede cambiar este nombre dentro `settings.yaml` en el
campo `pages`. Para el ejemplo sería:

    # archivo: settings.yaml

    pages:
      services:
        root: 's' #Ahora en la url aparecería como /s/...

# Globals

Puedes declarar variables globales disponibles en cualquier plantilla dentro del archivo `globals.yaml`.
Para sobreescribir estos valores desde cualquier otra página, se tiene que declarar un objecto con las variables
que se quieran sobreescribir.

    # archivo globals.yaml
    sitename: ...
    title: ...
    global_var: ...

    # archivo servicio.yaml
    globals:
      global_var: ...

Hay variables globales que para ser sobreescritas no requieren ser declaradas dentro de un objeto globals:
    - title
    - meta_description

# Title
1. 

# Slug
1. Si `slug` está declarado, El valor de la variable slug (hay que tener cuidado con los conflictos).
2. Si `title` está declarado, el title como slug.
3. Si `title` no está declarado, el nombre del archivo como slug.
